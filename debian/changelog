django-wkhtmltopdf (3.4.0-3) unstable; urgency=medium

  * QA Upload
  * trim py2+3 boilerplate
  * patch-out python3-six

 -- Alexandre Detiste <tchet@debian.org>  Sun, 17 Mar 2024 13:12:40 +0100

django-wkhtmltopdf (3.4.0-2) unstable; urgency=medium

  * Orphan the package, see #1063033
    - Remove myself from uploaders and update maintainer to Debian QA Group
    - Update Vcs-* to Debian group
  * Add d/source/options extend-diff-ignore to fix dpkg-source failure due to
    local changes (python package metadata regeneration) (Closes: #1044928)

 -- Scott Kitterman <scott@kitterman.com>  Sun, 04 Feb 2024 11:45:59 -0500

django-wkhtmltopdf (3.4.0-1) unstable; urgency=medium

  * New upstream release
  * Bump standards-version to 4.6.1 without further change

 -- Scott Kitterman <scott@kitterman.com>  Mon, 24 Oct 2022 18:27:17 -0400

django-wkhtmltopdf (3.3.0-3) unstable; urgency=medium

  [ Debian Janitor ]
  * Bump debhelper from old 12 to 13.
  * Update standards version to 4.6.0, no changes needed.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Thu, 26 May 2022 10:27:22 +0100

django-wkhtmltopdf (3.3.0-2) unstable; urgency=medium

  [ Debian Janitor ]
  * Bump debhelper from deprecated 9 to 12.
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.
  * Update standards version to 4.5.0, no changes needed.

  [ Ondřej Nový ]
  * d/control: Update Maintainer field with new Debian Python Team
    contact address.
  * d/control: Update Vcs-* fields with new Debian Python Team Salsa
    layout.

 -- Sandro Tosi <morph@debian.org>  Mon, 25 Apr 2022 12:20:30 -0400

django-wkhtmltopdf (3.3.0-1) unstable; urgency=medium

  * New upstream release
  * Add python3-six to depends
  * Bump standards-version to 4.4.1 without further change

 -- Scott Kitterman <scott@kitterman.com>  Sun, 19 Jan 2020 08:31:59 -0500

django-wkhtmltopdf (3.2.0-2) unstable; urgency=medium

  * Team upload.
  * Use debhelper-compat instead of debian/compat.
  * Drop Python 2 support.

 -- Ondřej Nový <onovy@debian.org>  Thu, 25 Jul 2019 14:18:46 +0200

django-wkhtmltopdf (3.2.0-1) unstable; urgency=medium

  [ Scott Kitterman ]
  * Add missing depends on python/python3-django
  * New upstream release
  * Convert from git-dpm to patches unapplied format
  * Bump standards-version to 4.1.4 without further change

  [ Ondřej Nový ]
  * d/control: Set Vcs-* to salsa.debian.org
  * d/copyright: Use https protocol in Format field
  * d/watch: Use https protocol

 -- Scott Kitterman <scott@kitterman.com>  Mon, 18 Jun 2018 14:44:13 -0400

django-wkhtmltopdf (3.1.0-1) unstable; urgency=medium

  * Initial release. (Closes: #843924)
  * Add missing LICENSE file

 -- Scott Kitterman <scott@kitterman.com>  Thu, 10 Nov 2016 15:16:01 -0500
